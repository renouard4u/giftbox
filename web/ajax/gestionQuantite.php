<?php
/**
 * Created by PhpStorm.
 * Date: 06/01/2017
 * Time: 19:18
 */

session_start();
if ($_POST['data'] === 'plus') {
    $_SESSION['coffret'][$_POST['id']] ++;
}
else if($_POST['data'] === 'moins'){
    $_SESSION['coffret'][$_POST['id']] --;
}
else if($_POST['data'] === 'supprimer') {
    $res = [];
    foreach ($_SESSION['coffret'] as $key=>$value) {
        if ($key != $_POST['id']) {
            $res[$key] = $value;
        }
        $_SESSION['coffret'] = $res;
    }
}

echo 'ok';