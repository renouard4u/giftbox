/**
 * Created by Clément on 06/01/2017.
 */

document.ready = init();

function init() {

    $('.plus').on('click', function () {
        $(this).parent().parent().children(".quantite").text(parseInt($(this).parent().parent().children(".quantite").text()) + 1);
        $.ajax({
            url: "web/ajax/gestionQuantite.php",
            method: 'POST',
            data: {data: 'plus', id: $(this).parent().parent().children(".quantite").attr('id')}
        }).done(function (data) {
            prix();
            console.log(data);
        }).error(function (data) {
            console.log(data)
        })
    })

    $('.moins').on('click', function () {
        if (parseInt($(this).parent().parent().children(".quantite").text()) - 1 >= 1) {
            $(this).parent().parent().children(".quantite").text(parseInt($(this).parent().parent().children(".quantite").text()) - 1);
            $.ajax({
                url: "web/ajax/gestionQuantite.php",
                method: 'POST',
                data: {data: 'moins', id: $(this).parent().parent().children(".quantite").attr('id')}
            }).done(function (data) {
                console.log(data);
                prix();
            }).error(function (data) {
                console.log(data)
            })
        }
    })

    $('.supprimer').on('click', function () {
        $.ajax({
            url: "web/ajax/gestionQuantite.php",
            method: 'POST',
            data: {data: 'supprimer', id: $(this).parent().parent().children(".quantite").attr('id')}
        }).done(function (data) {
            console.log(data);
            prix()
        }).error(function (data) {
            console.log(data)
        })
        $(this).parent().parent().empty();
    });


    function prix() {
        var prix = 0;
        $('.quantite').each(function () {
            prix += parseInt($(this).parent().children(".prix").text()) * parseInt($(this).text());
        })
        $(".total").text("Total : "  + " " + prix + "€");
    }

    prix();

}