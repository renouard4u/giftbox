<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 03/01/2017
 * Time: 11:43
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;


class Contient extends Model
{

    protected $table = 'contient';
    protected $primaryKey = 'id_contient';
    public $timestamps = false;

    public static function listerCadeau($idc){
        $q = Contient::select('id_prestation','quantite', 'statut')->where('id_coffret','=',$idc)->get();
        return $q;
    }


    public static function listerStatut($idc){
        $q = Contient::select('id_prestation','statut')->where('id_coffret','=',$idc)->get();
        return $q;
}


    public static function donnerContient($idc,$idPresta){
        $c = Contient::where("id_prestation","=",$idPresta)->where("id_coffret","=",$idc)->first();
        return $c;
    }

}