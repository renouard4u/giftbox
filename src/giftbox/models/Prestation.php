<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 06/12/2016
 * Time: 11:46
 */

namespace giftbox\models;

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function listerPrestations($tri){
        switch ($tri){
            case 'asc':
                $q = Prestation::orderBy('prix','asc')->with('categorie')->get();
                break;
            case 'desc':
                $q = Prestation::orderBy('prix','desc')->with('categorie')->get();
                break;
            default :
                $q = Prestation::orderBy('id','asc')->with('categorie')->get();
                break;
        }
        return $q;
    }

    public static function donnerPrestation($idp){
        $q = Prestation::find($idp);
        return $q;
    }

    public static function listerPrestationCategorieDonnee($idc, $tri){
        switch ($tri){
            case 'asc':
                $q = Prestation::where('cat_id','=',$idc)->orderBy('prix','asc')->get();
                break;
            case 'desc':
                $q = Prestation::where('cat_id','=',$idc)->orderBy('prix','desc')->get();
                break;
            default :
                $q = Prestation::where('cat_id','=',$idc)->orderBy('id','asc')->get();
                break;
        }
        return $q;
    }

    public function categorie(){
        return $this->belongsTo('\giftbox\models\Categorie','cat_id');
    }


}