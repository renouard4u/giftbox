<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 17/01/2017
 * Time: 18:45
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;


class Cagnotte extends Model
{

    protected $table = 'cagnotte';
    protected $primaryKey = 'id_cagnotte';
    public $timestamps = false;

    public static function verifierExistenceCagnotte($idCa, $tok){
        $c = Cagnotte::where("id_cagnotte", "=", $idCa)->where("code_cagnotte", "=", $tok)->first();
        return $c;
    }

    public static function donnerIdCoffret($idCa){
        $idc = Cagnotte::select("id_coffret")->where("id_cagnotte","=",$idCa)->first();
        return $idc;
    }

    public static function verifierExistenceCagnotteGestion($idCa, $tok){
        $c = Cagnotte::where("id_cagnotte", "=", $idCa)->where("code_gestionCagnotte", "=", $tok)->first();
        return $c;
    }

}