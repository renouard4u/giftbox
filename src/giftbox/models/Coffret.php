<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 03/01/2017
 * Time: 11:08
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;


class Coffret extends Model
{
    protected $table = 'coffret';
    protected $primaryKey = 'idcoff';
    public $timestamps = false;

    public static function verifierExistenceCadeau($idc, $tok){
        $q = Coffret::where('idcoff','=',$idc,'and','code_coffretOffert','=',$tok)->first();
        return $q;
       
    }

    public static function verifierExistenceCoffret($idc, $tok){
        $q = Coffret::where('idcoff','=',$idc,'and','code_coffretGestion','=',$tok)->first();
        return $q;
    }

    public static function donnerMdp($idc){
        $q = Coffret::select('mdp')->where('idcoff','=',$idc)->first();
        return $q;
    }

    public static function recupererCoffret($idc){
        $q = Coffret::where('idcoff','=',$idc)->first();
        return $q;
    }

}