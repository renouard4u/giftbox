<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 17/01/2017
 * Time: 18:46
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class Participe extends Model
{

    protected $table = 'participe';
    protected $primaryKey = 'id_participant';
    public $timestamps = false;



}