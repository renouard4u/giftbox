<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 17/01/2017
 * Time: 19:08
 */

namespace giftbox\controllers;
use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\Contient;
use giftbox\models\Cagnotte;
use giftbox\models\Participe;
use giftbox\views\CagnotteView;



class CagnotteController
{

public function participer($idCa, $tok){
    if(isset($idCa) && isset($tok)){
        $test = Cagnotte::verifierExistenceCagnotte($idCa, $tok);
        if(isset($test)){
            $c = Coffret::recupererCoffret($test->id_coffret);
            if($c->statut == "non paye") {
                $caV = new CagnotteView();
                $prestasQ = $this->listerPresta($idCa);
                $tabPrestas = [];
                foreach ($prestasQ as $key => $value) {
                    $tabPrestas[] = Prestation::donnerPrestation($key);
                }
                $caV->renderCagnotteParticipation(null, $tabPrestas, $prestasQ);
            }
            else{
                $caView = new CagnotteView();
                $caView ->renderCagnotteFermee() ;
            }
        }
    }
}

public function verifierParticipation($idCa, $tok){
    if(isset($idCa) && isset($tok)) {

        $test = Cagnotte::verifierExistenceCagnotte($idCa, $tok);
        if (isset($test)) {
            $c = Coffret::recupererCoffret($test->id_coffret);
            if($c->statut == "non paye"){
                if(isset($_POST['nomPart']) && isset($_POST['prenomPart']) && isset($_POST['montantPart'])) {
                    $nomPart = $_POST['nomPart'];
                    $prenomPart = $_POST['prenomPart'];
                    $montantPart = $_POST['montantPart'];
                    $errors = array();
                    if ($nomPart != filter_var($nomPart, FILTER_SANITIZE_STRING)) {
                        array_push($errors, "Nom invalide, merci de corriger");
                    }
                    if ($prenomPart != filter_var($prenomPart, FILTER_SANITIZE_STRING)) {
                        array_push($errors, "Prénom invalide, merci de corriger");
                    }
                    if ($montantPart != filter_var($montantPart, FILTER_SANITIZE_NUMBER_INT) || $montantPart < 1) {
                        array_push($errors, "Montant invalide, merci de corriger");
                    }

                    $caView = new CagnotteView();

                    if (sizeof($errors) == 0) {
                        $_POST['nomPart'] = filter_var($nomPart, FILTER_SANITIZE_STRING);
                        $_POST['prenomPart'] = filter_var($prenomPart, FILTER_SANITIZE_STRING);
                        $_POST['montantPart'] = filter_var($montantPart, FILTER_SANITIZE_NUMBER_INT);

                        $p = new Participe();
                        $p->id_cagnotte = $idCa;
                        $p->Nom = $_POST['nomPart'];
                        $p->Prenom = $_POST['prenomPart'];
                        $p->montant = $_POST['montantPart'];
                        $p->save();

                        $test->totalDonations += $_POST['montantPart'];
                        $test->save();

                        $caView->renderRemercier();
                    } else {
                        $prestasQ = $this->listerPresta($idCa);
                        $tabPrestas = [];
                        foreach ($prestasQ as $key => $value) {
                            $tabPrestas[] = Prestation::donnerPrestation($key);
                        }
                        $caView->renderCagnotteParticipation($errors, $tabPrestas, $prestasQ);
                    }
                }

            }else{
                $caView = new CagnotteView();
                $caView ->renderCagnotteFermee() ;
            }

        }
    }
}

public function listerPresta($idCa){
    $idCoff = Cagnotte::donnerIdCoffret($idCa)->id_coffret;
    $tabPresta = Contient::listerCadeau($idCoff);
    $prestas = [];
    foreach($tabPresta as $val){
        $test = $val->id_prestation ;
        $prestas[$test]=$val->quantite;
    }
    return $prestas;
}


public function demanderMdpCagnotte($idCa, $tok){
    if (isset($idCa) && isset($tok)){
        $test = Cagnotte::verifierExistenceCagnotteGestion($idCa, $tok);
        if (isset($test)){
            $caV = new CagnotteView();
            $caV->renderDemanderMdpCagnotte(null);
        }
    }
}

public function verifierMdpCagnotte($idCa, $tok) {
    if (isset($idCa) && isset($tok)){
        $test = Cagnotte::verifierExistenceCagnotteGestion($idCa, $tok);
        if (isset($test)){
            if(isset($_POST['mdpCagnotte'])){
                $mdp = $_POST['mdpCagnotte'];
                $idCoff = Cagnotte::donnerIdCoffret($idCa)->id_coffret;
                $pass = Coffret::donnerMdp($idCoff)->mdp;
                if(password_verify($mdp, $pass)) {
                    $this->gererCagnotte($idCa, $tok);
                }else{
                    $errors = "Mot de passe incorrect, veuillez essayer à nouveau";
                    $caV = new CagnotteView();
                    $caV->renderDemanderMdpCagnotte($errors);
                }
            }

        }
    }
}

public function gererCagnotte($idCa, $tok){
    if(isset($idCa) && isset($tok)) {
        $test = Cagnotte::verifierExistenceCagnotteGestion($idCa, $tok);
        if (isset($test)) {
            $caV = new CagnotteView();
            $prestasQ = $this->listerPresta($idCa);
            $tabPrestas = [];
            foreach ($prestasQ as $key => $value) {
                $tabPrestas[] = Prestation::donnerPrestation($key);
            }
            $caV->renderCagnotteGestion(null, $tabPrestas, $prestasQ, $test->objectif, $test->totalDonations);
        }
    }
}


public function participerGererCagnotte($idCaGest,$tok){
    if(isset($idCaGest) && isset($tok)){

        $test = Cagnotte::verifierExistenceCagnotteGestion($idCaGest, $tok);
        if(isset($test)){
            $c = Coffret::recupererCoffret($test->id_coffret);
            if($c->statut == "non paye") {
                $caV = new CagnotteView();
                $prestasQ = $this->listerPresta($idCaGest);
                $tabPrestas = [];
                foreach ($prestasQ as $key => $value) {
                    $tabPrestas[] = Prestation::donnerPrestation($key);
                }
                $caV->renderCagnotteParticipation(null, $tabPrestas, $prestasQ);
            }else{
                $caView = new CagnotteView();
                $caView ->renderCagnotteFermee() ;
            }
        }
    }

}

    public function verifierParticipationGestion($idCa, $tok){
        if(isset($idCa) && isset($tok)) {

            $test = Cagnotte::verifierExistenceCagnotteGestion($idCa, $tok);
            if (isset($test)) {
                if(isset($_POST['nomPart']) && isset($_POST['prenomPart']) && isset($_POST['montantPart'])){
                    $nomPart = $_POST['nomPart'];
                    $prenomPart = $_POST['prenomPart'];
                    $montantPart= $_POST['montantPart'];
                    $errors = array();
                    if ($nomPart != filter_var ( $nomPart, FILTER_SANITIZE_STRING )) {
                        array_push ( $errors, "Nom invalide, merci de corriger" );
                    }
                    if ($prenomPart != filter_var ( $prenomPart, FILTER_SANITIZE_STRING )) {
                        array_push ( $errors, "Prénom invalide, merci de corriger" );
                    }
                    if ($montantPart != filter_var ( $montantPart, FILTER_SANITIZE_NUMBER_INT ) || $montantPart<1) {
                        array_push ( $errors, "Montant invalide, merci de corriger" );
                    }

                    $caView = new CagnotteView();
                    $prestasQ = $this->listerPresta($idCa);
                    $tabPrestas = [];
                    foreach($prestasQ as $key=>$value){
                        $tabPrestas[] = Prestation::donnerPrestation($key);
                    }

                    if (sizeof($errors) == 0){
                        $_POST['nomPart'] = filter_var($nomPart, FILTER_SANITIZE_STRING);
                        $_POST['prenomPart'] = filter_var($prenomPart, FILTER_SANITIZE_STRING);
                        $_POST['montantPart'] = filter_var($montantPart, FILTER_SANITIZE_NUMBER_INT);

                        $p = new Participe();
                        $p->id_cagnotte = $idCa;
                        $p->Nom = $_POST['nomPart'];
                        $p->Prenom = $_POST['prenomPart'];
                        $p->montant = $_POST['montantPart'];
                        $p->save();

                        $test->totalDonations += $_POST['montantPart'];
                        $test->save();

                        $caView->renderCagnotteGestion($errors, $tabPrestas, $prestasQ, $test->objectif, $test->totalDonations);
                    }
                    else{

                        $caView->renderCagnotteParticipation($errors, $tabPrestas, $prestasQ);
                    }

                }

            }
        }
    }


    public function cloturerCagnotte($idCa, $tok){
        $caView = new CagnotteView();
        $prestasQ = $this->listerPresta($idCa);
        $tabPrestas = [];
        $error = array();
        foreach($prestasQ as $key=>$value){
            $tabPrestas[] = Prestation::donnerPrestation($key);
        }
        if(isset($idCa) && isset($tok)) {
            $test = Cagnotte::verifierExistenceCagnotteGestion($idCa, $tok);
            $c = Coffret::recupererCoffret($test->id_coffret);
            if (isset($test)) {
                if($test->totalDonations >= $test->objectif){
                    $c = Coffret::recupererCoffret($test->id_coffret);
                    $c -> statut = 'paye';
                    $c -> save();


                    $caView -> renderCloturerCagnotte($c->idcoff,$c->code_coffretOffert,$c->code_coffretGestion);
                }
                else{
                    $error[] = "L'objectif n'a pas encore été atteint";
                    $caView -> renderCagnotteGestion($error,$tabPrestas,$prestasQ,$test->objectif,$test->totalDonations );
                }
            }
        }
    }
}