<?php

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 14/12/2016
 * Time: 14:19
 */

namespace giftbox\controllers;
use giftbox\models\Coffret;
use giftbox\models\Contient;
use giftbox\models\Prestation;
use giftbox\models\Cagnotte;
use giftbox\models\Participe;
use giftbox\views\CagnotteView;
use giftbox\views\CoffretView;

class CoffretController
{

    public static function listerCoffret(){
        $res = array();
        if(isset($_SESSION ["coffret"]))
            foreach ($_SESSION['coffret'] as $key=>$value){
                $presta = Prestation::find($key);
                $res[] = $presta;
            }
        $coffretView = new CoffretView($res);
        echo $coffretView->render();
    }

    public function donnerInfo() {
        $prestations = [];
        if(isset($_SESSION['coffret'])) {
            foreach ($_SESSION['coffret'] as $key => $value) {
                array_push($prestations, Prestation::donnerPrestation($key));
            }
        }
        $categories = [];
        foreach ($prestations as $key=>$value) {
            if(!in_array($value->cat_id, $categories))
                array_push($categories, $value->cat_id);
        }

        if(sizeof($prestations) < 2 || sizeof($categories) < 2) {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('coffret'));
        }

        $coffretView = new CoffretView(null);
        $coffretView->renderForm();
    }

    public function verifierInfo() {
        //securiser input

        if (isset($_POST['nom']))
            $nom = $_POST['nom'];
        if (isset($_POST['prenom']))
            $prenom = $_POST['prenom'];
        if (isset($_POST['email']))
            $email = $_POST['email'];
        if (isset($_POST['message']))
            $message = $_POST['message'];
        if (isset($_POST['jCad']))
            $jCad = $_POST['jCad'];
        if (isset($_POST['mCad']))
            $mCad = $_POST['mCad'];
        if (isset($_POST['aCad']))
            $aCad = $_POST['aCad'];
        $paiement = $_POST['paiement'];
        $errors = [];

        if ($nom != filter_var ( $nom, FILTER_SANITIZE_STRING )) {
            array_push ( $errors, "Nom invalide, merci de corriger" );
        }
        if ($prenom != filter_var ( $prenom, FILTER_SANITIZE_STRING )) {
            array_push ( $errors, "Prenom invalide, merci de corriger" );
        }
        if ($email != filter_var($email, FILTER_SANITIZE_STRING) || !filter_var($email, FILTER_VALIDATE_EMAIL)){
            array_push ( $errors, "Email invalide, merci de corriger" );
        }
        if ($aCad != filter_var($aCad, FILTER_SANITIZE_NUMBER_INT) || $aCad < date('Y')){
            array_push( $errors, "Année d'ouverture invalide, merci de corriger");
        }else{
            if ($mCad != filter_var($mCad, FILTER_VALIDATE_INT) || ($aCad == date('Y') && $mCad < date('n')) || $mCad>12) {
                array_push($errors, "Mois d'ouverture invalide, merci de corriger");
            }
            else{
                if ($jCad != filter_var($jCad, FILTER_VALIDATE_INT) || ($aCad == date('Y') && $mCad == date('n') && $jCad< date('j') || $jCad> cal_days_in_month(CAL_GREGORIAN, $mCad, $aCad))){
                    array_push( $errors, "Jour d'ouverture invalide, merci de corriger");
                }
            }
        }


        $mdp = NULL;

        if (isset($_POST['mdp'])&& isset($_POST['mdp2'])) {
            $mdp = $_POST['mdp'];
            $mdp2 = $_POST['mdp2'];
            if($mdp == $mdp2){
                if ($mdp != filter_var($mdp, FILTER_SANITIZE_STRING)){
                    array_push ( $errors, "mot de passe invalide, merci de corriger" );
                }
            }else{
                array_push ( $errors, "pas les mêmes mots de passe, merci de corriger" );
            }

            $mdp = filter_var($mdp,FILTER_SANITIZE_STRING);

        }

        if($mdp != null) {
            $mdp = password_hash($mdp, PASSWORD_DEFAULT, Array(
                'cost' => 12
            ));
            $_POST['mdp'] = $mdp;
        }


        if (sizeof ( $errors ) == 0) {
            $_POST['nom'] = filter_var($nom, FILTER_SANITIZE_STRING);
            $_POST['prenom'] = filter_var($prenom, FILTER_SANITIZE_STRING);
            $_POST['email'] = filter_var($email, FILTER_SANITIZE_EMAIL);
            $_POST['message'] = filter_var($message, FILTER_SANITIZE_SPECIAL_CHARS);
            $_POST['jCad'] = filter_var($jCad, FILTER_SANITIZE_NUMBER_INT);
            $_POST['mCad'] = filter_var($mCad, FILTER_SANITIZE_NUMBER_INT);
            $_POST['aCad'] = filter_var($aCad, FILTER_SANITIZE_NUMBER_INT);

            $tab = array();
            foreach ($_SESSION['coffret'] as $key=>$value){
                $presta = Prestation::find($key);
                $tab[] = $presta;
            }

            $coffretView = new CoffretView($tab);
            $coffretView->renderRecapitulatif();
        }
        else {
            $coffretView = new CoffretView(null);
            $coffretView->renderForm($errors);
        }
    }

    public function enregistrerCoffret()
    {
        $tabURL = array();


        if (isset($_SESSION['coffret'])) {
            $factory = new \RandomLib\Factory;
            $generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
            $tokenGest = $generator->generateString(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            $tokenCadeau = $generator->generateString(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            $tabURL['tc']=$tokenCadeau;
            $tabURL['tg']=$tokenGest;
            $c = new Coffret();
            $c->code_coffretGestion = $tokenGest;
            $c->code_coffretOffert = $tokenCadeau;
            $c->nom = $_POST['nom'];
            $c->prenom = $_POST['prenom'];
            $c->mail = $_POST['email'];
            $c->mess = $_POST['message'];
            $c->openDate = $_POST['aCad'].'-'.$_POST['mCad'].'-'.$_POST['jCad'];
            if($_POST['paiement'] == 'classique'){
                $c->statut = "paye";
            }else{
                $c->statut = "non paye";
            }
            $c->mdp = $_POST['mdp'];
            $c-> montant = $_SESSION['TOTAL'];

            $c->save();
            $tabURL['idc']=$c->idcoff;
            foreach ($_SESSION['coffret'] as $key => $value) {
                $con = new Contient();
                $con->id_coffret = $c->idcoff;
                $con->id_prestation = $key;
                $con->quantite = $value;
                $con->statut = "Non ouvert";
                $con->save();
            }

            $tabURL['idc']=$c->idcoff;
            $tabURL['montant'] = $c->montant;
            session_unset();

            $_SESSION['infos'] = $tabURL;

            if($_POST['paiement'] == 'classique'){
                $coffretView = new CoffretView(null);
                $coffretView->renderCoffretFini($c->idcoff,$tokenCadeau, $tokenGest);
            }else if($_POST['paiement'] == 'cagnotte'){
                $tokenCag = $generator->generateString(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
                $tokenCagGest = $generator->generateString(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
                $ca = new Cagnotte();
                $ca->code_cagnotte = $tokenCag;
                $ca->code_gestionCagnotte = $tokenCagGest;
                $ca->id_coffret = $tabURL['idc'];
                $ca->objectif = $tabURL['montant'];
                $ca->totalDonations = 0;
                $ca->save();

                $_SESSION['tokCa'] = $tokenCag;
                $_SESSION['tokCaGest'] = $tokenCagGest;
                $_SESSION['idCa'] = $ca->id_cagnotte;

                $cagnotteView = new CagnotteView();
                $cagnotteView->renderCoffretCagnotte();
            }
        }
    }


    public function afficherCadeau($idc, $tok){
        $tab = array();
        $prestaQ = null;
        $statutP = array();
        if(isset($idc) && isset($tok)){
            $test = Coffret::verifierExistenceCadeau($idc, $tok);
            if(isset($test)){
                $prestaQ = Contient::listerCadeau($idc);
                foreach($prestaQ as $val){
                    $id = $val->id_prestation ;
                    $statutP[$id]= $val->statut;
                    $q = $val->quantite ;
                    $tab[$id] = $q ;
                }
            }
        }


        $coffV = new CoffretView(null);


        $coffV->renderCadeau($tab, $test, $idc, $statutP);
    }

}