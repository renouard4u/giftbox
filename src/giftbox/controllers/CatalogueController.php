<?php

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 14/12/2016
 * Time: 14:19
 */

namespace giftbox\controllers;
use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\views\CatalogueView;

class CatalogueController
{
    public $app;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    public function listePrestations(){
        $liste = Prestation::listerPrestations($this->app->request->get('tri'));
        $cv = new CatalogueView($liste, 'LIST_PRESTA_VIEW',true);
        echo $cv->render();
    }


    public function listePrestationsCategorieDonnee($idc){
        $liste = Prestation::listerPrestationCategorieDonnee($idc, $this->app->request->get('tri'));
        $cv = new CatalogueView($liste, 'LIST_PRESTACAT_VIEW',true);
        echo $cv->render();
    }
}