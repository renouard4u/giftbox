<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 14/01/2017
 * Time: 10:58
 */

namespace giftbox\controllers;
use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\Contient;
use giftbox\views\GestionView;


class GestionController
{
    public function demanderMdp($idc, $tok){
        if(isset($idc) && isset($tok)){
            $test = Coffret::verifierExistenceCoffret($idc, $tok);
            if(isset($test)){
                $gestV = new GestionView();
                $gestV->renderDemandeMdp(null);
            }
        }
    }

    public function verifierMdp($idc, $tok){
        if(isset($idc) && isset($tok)){
            $test = Coffret::verifierExistenceCoffret($idc, $tok);
            if(isset($test)){
                if(isset($_POST['motdepasse'])){
                    $mdp = $_POST['motdepasse'];
                    $gestV = new GestionView();
                    if(password_verify($mdp, $test->mdp)){
                        $this->gererCadeau($idc,$tok);
                    }else{
                        $error = "Mot de passe incorrect, veuillez essayer à nouveau";
                        $gestV->renderDemandeMdp($error);
                    }
                }
            }
        }
    }

    public function gererCadeau($idc, $tok){
        $tab = array();
        $test = array();
        if(isset($idc) && isset($tok)){
                $prestaQ = Contient::listerCadeau($idc);
                foreach($prestaQ as $val){
                    $id = $val->id_prestation ;
                    $q = $val->quantite ;
                    $tab[$id] = $q ;
                }
                $statutPrestas = Contient::listerStatut($idc);
                foreach ($statutPrestas as $val){
                    $id = $val->id_prestation ;
                    $s  = $val->statut ;
                    $test[$id] = $s ;
                }
        }
        $coffV = new GestionView(null);
        $coffV->renderGestion($tab, $test);
    }

    public function changerStatut($idc, $idp){
        if (isset($idc) && isset($idp)) {
            $c = Contient::donnerContient($idc,$idp);
            if (isset($c) && $c->statut != "Ouvert"){
                $c->statut = "Ouvert";
                $c->save();
            }
        }
    }
}