<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 04/01/2017
 * Time: 11:24
 */

namespace giftbox\controllers;
use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\views\PrestationView;


class PrestationController
{


    public function detailPrestation($idp){
        $presta = Prestation::donnerPrestation($idp);
        $cv = new PrestationView($presta, 'PRESTA_VIEW');
        echo $cv->render();
    }

    public function ajouterPrestation($id){
        if(!isset($_SESSION['coffret'])){
            $_SESSION['coffret'] = [];
            $_SESSION['coffret'][$id]=1;
        }else{
            if(array_key_exists($id,$_SESSION['coffret'])){
                $_SESSION['coffret'][$id] = $_SESSION['coffret'][$id]+1;
            }else{
                $_SESSION['coffret'][$id]=1;
            }
        }
        $this->detailPrestation($id);
    }
}