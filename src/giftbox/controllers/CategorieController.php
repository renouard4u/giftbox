<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/01/2017
 * Time: 14:56
 */

namespace giftbox\controllers;


use giftbox\models\Categorie;
use giftbox\views\CategorieView;


class CategorieController
{

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }


    public function listeCategories(){
        $liste = Categorie::listerCategories();
        $cv = new CategorieView($liste);
        echo $cv->render();
    }

}