<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 14/01/2017
 * Time: 10:57
 */

namespace giftbox\views;

use giftbox\models\Prestation;
class GestionView
{
    public $URI;

    public function __construct(){
        $this->URI = \Slim\Slim::getInstance()->request->getRootUri();
    }

    public function htmlDemandeMdp(){
        $aff = '<form method="POST">    
                    <label> 
                        <b>Indiquez votre mot de passe pour accéder à la gestion du coffret :</b>
                        <input type="password" name="motdepasse" placeholder="Mot de passe du coffret" required/>
                    </label>
                        <input type="submit" value="Valider"/>
                </form>';
        return $aff;
    }

    public function renderDemandeMdp($error){
        $content ='';
        if (isset($error)){
            $content .= "<p class=\"red-text\">$error</p>";
        }
        $content .= $this->htmlDemandeMdp();
        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
            <h1><b>Giftbox</b></h1>
        </div>
        $content
    </body>
</html>


END;

        echo $html;
    }


    public function htmlGestionCoffret($listPresta , $test){
        $prestas ="";
        $nbPrestas = 0 ;
        $nbOuverts = 0 ;
        foreach ($listPresta as $key=>$value){
            $p = Prestation::donnerPrestation($key);
            $nbPrestas ++ ;
            $statut = $test[$key];
            if ($statut == "Ouvert"){
                $nbOuverts++ ;
            }
            $prestas.=
                "<li><br>
            <div class=\"collapsible-header\"><span class=\"badge\">Quantité : $value &nbsp</span><i class=\"material-icons\">redeem</i>$p->nom : $statut</div><br>
            <div class=\"collapsible-body\">
            <div class=\"pres\"><img src=\"$this->URI/web/img/$p->img\" style =\"width:100%;height:100%;\"></div>
            <div class='pres2'><h4><b>$p->nom</b></h4>    <p>$p->descr</p> <p><b>$p->prix € / unité</b></p></div></div></div><br>
            </li><br>";
        }
        if($nbOuverts == 0){
            $etat = "Ce coffret n'a pas encore été ouvert." ;
        }
        else {
            if ($nbOuverts < $nbPrestas) {
                $etat = "Ce coffret est en cours d'ouverture; le destinataire n'a pas encore visualisé toutes les prestations.";
            } else {
                $etat = "Ce coffret a été entièrement visualisé.";
            }
        }
        $content ="<p id='descrCadeau'>$etat 
                    <ul class=\"collapsible popout\" data-collapsible=\"accordion\"><br>";
        return $content.$prestas ;
    }


    public function renderGestion($listPresta , $test ){
        $content = $this->htmlGestionCoffret($listPresta, $test);
        $html = <<<END
        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
    <body>
    <div class="test2">
        <h1><b>Giftbox</b></h1>
        </div>
    $content
    </body>
</html>
END;

        echo $html ;
    }


}