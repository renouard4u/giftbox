<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 17/01/2017
 * Time: 19:08
 */

namespace giftbox\views;


class CagnotteView
{
    public $URI;

    public function __construct()
    {
        $this->URI = \Slim\Slim::getInstance()->request->getRootUri();
    }

    public function renderCoffretCagnotte()
    {
        $idCa = $_SESSION['idCa'];
        $tokCa = $_SESSION['tokCa'];
        $tokCaGest = $_SESSION['tokCaGest'];
        $host = $_SERVER['HTTP_HOST'];
        $html = <<<END
            
            <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
   <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
         <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<p><b>THOMAS Clément</b></p>
		<p><b>MERLIN Paul</b></p>
		<p><b>DELAMARRE Quentin</b></p>
		<p><b>RENOUARD Quentin</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
        <div class="lien"><p>Merci pour votre achat.<br><br>Envoyez ce lien aux potentiels participants, pour qu'ils puissent contribuer :
        <br><a href="http://$host$this->URI/$idCa/cagnotte?token=$tokCa">http://$host$this->URI/$idCa/cagnotte?token=$tokCa</a>
        <br><br>Si vous désirez cloturer la cagnotte et/ou y participer, utilisez ce lien :<br>
        <a href="http://$host$this->URI/$idCa/gererCagnotte?token=$tokCaGest">http://$host$this->URI/$idCa/gererCagnotte?token=$tokCaGest</a></p></div>
  
   </body>
</html>
END;
        echo $html;

    }


    public function htmlDemanderInfoMontant()
    {
        $aff = '<form method="POST">
                    <label>
                        <b>Nom :</b>
                        <input type="text" name="nomPart" placeholder="ex : Dupont" required/>
                    </label>
                    <label>
                        <b>Prénom :</b>
                        <input type="text" name="prenomPart" placeholder="ex : Jean-Claude" required/>
                    </label>
                    <label>
                        <b>Montant :</b>
                        <br>La participation minimale est de 1€<br>Les centimes ne sont pas acceptés
                        <input type="number" name="montantPart" value="1" min="1" required/>
                        <br><br>
                    </label>
                    <input type="submit" value="Valider ma participation"/>
                    </form>';
        return $aff;
    }

    public function renderCagnotteParticipation($errors, $listPresta, $idpQ)
    {
        $err = '';
        if ($errors != null) {
            foreach ($errors as $value) {
                $err .= '<p class="red-text">' . $value . '</p>';
            }
        }
        $content = $this->htmlDemanderInfoMontant();
        $content .= "<br><ul class=\"collapsible popout\" data-collapsible=\"accordion\"><br>";
        foreach ($listPresta as $presta) {
            $qte = $idpQ[$presta->id];
            $content .= "<li><br>
            <div class=\"collapsible-header\"><span class=\"badge\" >Quantité : $qte &nbsp</span><i class=\"material-icons\">redeem</i>$presta->nom</div><br>
            <div class=\"collapsible-body\">
            <div class=\"pres\"><img src=\"$this->URI/web/img/$presta->img\" style =\"width:100%;height:60%\"></div>
            <div class='pres2'><h4><b>$presta->nom</b></h4>    <p>$presta->descr</p> <br></div></div></li>";
        }
        $content .= "</ul><br>";
        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
            <h1><b>Giftbox</b></h1>
        </div>
        $err
        $content
    </body>
</html>


END;

        echo $html;
    }


    public function renderRemercier()
    {
        $html = <<<END
            
            <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
   <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
         <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<p><b>THOMAS Clément</b></p>
		<p><b>MERLIN Paul</b></p>
		<p><b>DELAMARRE Quentin</b></p>
		<p><b>RENOUARD Quentin</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
        <div class="lien"><p>Le créateur du coffret vous remercie,<br>il sera informé de votre geste généreux et attentionné.</p></div>
  
   </body>
</html>
END;
        echo $html;
    }

    public function htmlDemanderMdpCagnotte()
    {
        $aff = '<form method="POST">    
                    <label> 
                        <b>Indiquez votre mot de passe pour accéder à la gestion de la cagnotte :</b>
                        <input type="password" name="mdpCagnotte" placeholder="Mot de passe de la cagnotte" required/>
                    </label>
                        <input type="submit" value="Accéder à la cagnotte"/>
                </form>';
        return $aff;
    }


    public function renderDemanderMdpCagnotte($errors)
    {
        $content = '';
        if (isset($errors)) {
            $content .= "<p class=\"red-text\">$errors</p>";
        }
        $content .= $this->htmlDemanderMdpCagnotte();
        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
        <title>Giftbox</title> 
    </head>
    <body>
        <div class="test2">
            <h1><b>Giftbox</b></h1>
        </div>
        $content
    </body>
</html>


END;

        echo $html;
    }

    public function renderCagnotteGestion($errors, $listPresta, $idpQ, $objectif, $total)
    {
        $idCa = $_SESSION['idCa'];
        $avancee = ($total / $objectif) * 100;
        $tokCaGest = $_SESSION['tokCaGest'];
        $content = '';
        $err = '';
        if ($errors != null) {
            foreach ($errors as $value) {
                $err .= '<p class="red-text">' . $value . '</p>';
            }
        }
        $content .= "<p class='white-text'>$total € sur $objectif €</p>
                    <div class=\"progress\">
                    <div class=\"determinate\" style=\"width :$avancee%\"></div>
                    </div>";
        $content .= "<a class=\"waves-effect waves-light btn\" href=\"$this->URI/$idCa/gererGagnotte/participer?token=$tokCaGest\" >Je souhaite participer</a>
        <a class=\"waves-effect waves-light btn\" href=\"$this->URI/$idCa/gererGagnotte/cloturer?token=$tokCaGest\" >Je souhaite cloturer la cagnotte</a>";

        $content .= "<br><ul class=\"collapsible popout\" data-collapsible=\"accordion\"><br>";
        foreach ($listPresta as $presta) {
            $qte = $idpQ[$presta->id];
            $content .= "<li><br>
            <div class=\"collapsible-header\"><span class=\"badge\" >Quantité : $qte &nbsp</span><i class=\"material-icons\">redeem</i>$presta->nom</div><br>
            <div class=\"collapsible-body\">
            <div class=\"pres\"><img src=\"$this->URI/web/img/$presta->img\" style =\"width:100%;height:60%\"></div>
            <div class='pres2'><h4><b>$presta->nom</b></h4>    <p>$presta->descr</p> <br></div></div></li>";
        }
        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
            <h1><b>Giftbox</b></h1>
        </div>
        $err
        $content
    </body>
</html>


END;

        echo $html;
    }

    public function renderCloturerCagnotte($idc,$tc,$tg)
    {
        $host = $_SERVER['HTTP_HOST'];

        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
    <body>
       <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
         <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
         <div class="lien"><p><b>LA CAGNOTTE A ETE CLOTUREE.</b><br>Merci pour votre achat.<br><br>Pour faire partager à la personne concernée, envoyé lui ce lien :
        <br><a href="http://$host$this->URI/$idc/cadeau?token=$tc">http://$host$this->URI/$idc/cadeau?token=$tc</a>
        <br><br>Si vous desirez modifier ou consulter ce coffret, vous pourrez le faire ici :<br>
        <a href="http://$host$this->URI/$idc/gerer?token=$tg">http://$host$this->URI/$idc/gerer?token=$tg</a></p></div>
    </body>
</html>
    

END;
        echo $html;
    }

    public function renderCagnotteFermee(){
        $html = <<< END

        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
    <body>
       <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
         <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        <h3 class="red-text"><b><br> LA CAGNOTTE A ETE CLOTUREE</b><p></h3>   </body>
</html>
    

END;
        echo $html;

    }
}