<?php

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 14/12/2016
 * Time: 16:25
 */

namespace giftbox\views;

class CatalogueView
{
    private $objectPresta;
    private $typeAff;
    public $URI;
    private $trie;
    private $route;


    public function __construct($o, $ta, $tr){
        $this->objectPresta = $o;
        $this->typeAff = $ta;
        $this->trie = $tr;
        $this->URI =\Slim\Slim::getInstance()->request->getRootUri();
        $this->route = '';
    }

    public function htmlListePrestations(){
        $affichage = '';
        foreach($this->objectPresta as $res){
            $affichage .= '<a href='.$this->URI.'/prestations/'.$res->id.'><div class="presta"><img src="'.$this->URI.'/web/img/'.$res->img.'" style ="width:100%; height:100%"  alt=""><br>'.'<b>'.$res->nom.'</b>'.'<br>Catégorie : '.$res->Categorie->nom .'<br>'.$res->prix.'€<br><br></div></a>';
        }
        $this->route = 'prestations';
        return $affichage;
    }


    public function htmlPrestationsCategorieDonnee(){
        $affichage = '';
        $tmp = '';
        foreach($this->objectPresta as $res){
            $affichage .= '<a href='.$this->URI.'/prestations/'.$res->id.'>'.'<div class="presta">'.'<img src="'.$this->URI.'/web/img/'.$res->img.'" style ="width:100%; height:100%" alt="">'.'<br><b>'.$res->nom.'</b><br>'.$res->prix.'€</div></a>';
            $tmp = $res->cat_id;
        }
        $this->route = 'categories/'.$tmp;
        return $affichage;
    }

    public function render(){
        switch($this->typeAff){
            case 'LIST_PRESTA_VIEW':
                $content = $this->htmlListePrestations();
                break;
            case 'PRESTA_VIEW':
                $content = $this->htmlPrestation();
                break;
            case 'LIST_PRESTACAT_VIEW':
                $content = $this->htmlPrestationsCategorieDonnee();
                break;
        }
        $nbGift=0;
        if(isset($_SESSION['coffret'])){
            foreach ($_SESSION['coffret'] as $produit=>$qt){
                $nbGift+=$qt;
            }

        }
            $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/coffret"><span>Ma Giftbox&nbsp&nbsp</span> <i class="material-icons">redeem</i><span class="shad">&nbsp$nbGift</span></a>
        </div>

      
<div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
END;
            if($this->trie) {
                $html .= <<<END
        <ul id="dropdown3" class="dropdown-content">
        <li><a href="$this->URI/$this->route?tri=asc">Prix croissant</a></li>
        <li><a href="$this->URI/$this->route?tri=desc">Prix décroissant</a></li>
        </ul>
        <a class="btn dropdown-button" href="" data-activates="dropdown3">Trier par<i class="mdi-navigation-arrow-drop-down right"></i></a>	 
END;
            }
             $html.= <<<END
        <div class ="test">
        $content
        </div>
    </body>

    </html>
END;

    return $html;
    }
}