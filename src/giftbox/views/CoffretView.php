<?php
/**
 * Created by PhpStorm.
 * User: Clément
 * Date: 27/12/2016
 * Time: 20:11
 */

namespace giftbox\views;

use giftbox\models\Contient;
use giftbox\models\Prestation;
use giftbox\controllers\CoffretController;

class CoffretView
{

    public $URI;
    private $route;
    private $prestas;

    public function __construct($res){
        $this->URI = \Slim\Slim::getInstance()->request->getRootUri();
        $this->route = '';
        $this->prestas = $res ;
    }


    public function htmlCoffret(){
        $affichage = '';
        $p = $this->prestas;
        foreach($p as $key=>$value){
          $affichage.= '
            <tr>
              <td>'.$value->nom.'</td>
              <td>'.$value->descr.'</td>
              <td class="prix">'.$value->prix.'€</td>
              <td><button class="boutonQte moins">-</button></td>
              <td id="'.$value->id . '"class="quantite">'.$_SESSION['coffret'][$value->id].'</td>
              <td><button class="boutonQte plus">+</button></td>
              <td><a class="supprimer btn-floating btn waves-effect waves-light red accent-4"><i class="material-icons">delete</i></a></td>
              </tr>';
        }

        $this->route = 'coffret';
        return $affichage;
    }

    public function render(){
        $content = $this->htmlCoffret();
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
         </div>
        
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
        
        
        <table class="striped centered">
        <thead>
          <tr>
              <th data-field="id">Prestation</th>
              <th data-field="price">Description</th>
              <th data-field="name">Prix Unitaire</th>
              <th date-field="name">  </th>
              <th data-field="price">Qté</th>
              <th date-field="name">  </th>
              <th date-field="name"></th>
          </tr>
        </thead>

        <tbody>
        $content

        </tbody>
      </table>  
       <div class="total"><p>Total</p></div>
       <a href="$this->URI/coffret/validation" class="btn btn-waves">valider<i class="material-icons right">done_all</i></a>
       <script type="text/javascript" src="web/js/gestionQuantite.js"></script>
    </body>
</html>
END;

        return $html;
    }

    public function renderForm($errors = null) {
        $content = '';
        $err ='';
        if ($errors != null) {
            foreach ($errors as $value) {
                $err .= '<p class="red-text">' . $value . '</p>';
            }
        }
        $nbGift=0;
        if(isset($_SESSION['coffret'])){
            foreach ($_SESSION['coffret'] as $produit=>$qt){
                $nbGift+=$qt;
            }

        }
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
        <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/coffret"><span>Ma Giftbox&nbsp&nbsp</span> <i class="material-icons">redeem</i><span class="shad">&nbsp$nbGift</span></a>
        </div>
        
        
        
            
<div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
        $content
       <div class="formulaire">
       $err
       <form method="POST">
            <label>
            <b>Nom :</b>
            <input type="text" name="nom" placeholder="ex : Dupont" required/>
            </label>
            <label>
            <b>Prénom :</b>
            <input type="text" name="prenom" placeholder="ex : Jean-Claude" required/>
            </label>
            <label><b>E-mail :</b>
            <input type="text" name="email" placeholder="ex : JCDupont@blabla.fr" required/>
            </label>
            <label><b>Mot de passe :</b>
            <input type="password" name="mdp" placeholder="Mot de passe du coffret" required/>
            </label>
            <label><b>Confirmez votre mot de passe :</b>
            <input type="password" name="mdp2" placeholder="Confirmez le mot de passe du coffret" required/>
             </label>
            <label><b>Votre message :</b>
            <textarea name="message" placeholder="Entrez votre message..." maxlength="1000"></textarea>
            </label>
            
            
            <label><b>Date d'ouverture du coffret cadeau :</b><br>
            Jours:
            <input type="number" name="jCad" value="1" min="1" max="31" required >
            Mois :
            <input type="number" name="mCad" value="1" min="1" max="12" required >
            Année :
            <input type="number" name="aCad" value="2017" min="2017" max="3000" required>
            </label>
            
            
            <label><b>Mode de paiement :</b><br>
            <input type="radio" name="paiement" value="classique" checked id="1"><label for="1">Paiement classique</label>
            <input type="radio" name="paiement" value="cagnotte"  id="2"><label for="2">Utiliser une cagnotte</label><br><br>
            </label>
            <input type="submit" value="Valider"/>
        </form>
        </div>
</body>
</html>
END;
        echo $html;
    }

    public function htmlform(){
        $content ="" ;
        $total = 0;
        foreach($this->prestas as $p){
            $total += $_SESSION['coffret'][$p->id]*$p->prix;
            $content.= '<tr><td>' . $p->nom . '</td><td>'.$p->prix . '€</td><td>'. $_SESSION['coffret'][$p->id] . '</td></tr>';
        }
        $_SESSION['TOTAL'] = $total ;
        $content .='</tbody></table> ';
        $content.= '<div class="total"> Total: ' . $total .'€ </div>';
        switch($_POST['paiement']) {
            case 'cagnotte':
                $content.="<p class='white-text'>Vous avez choisi le mode de paiment par cagnotte.<br></p>
                        <form method=\"POST\" action=\"$this->URI/coffret/liens\">";
                foreach ($_POST as $key => $value) {
                    $content .= '<input type="hidden" name="'.$key.'" value="' . $value . '" />';
                }
                $content .='<input type="submit"  value="continuer"/></div></form><script type="text/javascript" src="web/js/gestionQuantite.js"></script>';
                break;
            case 'classique':
                $content .= '<div class="formulaire">
            <p>Vous avez choisi le mode de paiement classique.<br> Veuillez indiquez vos coordonnees bancaires ci-dessous:</p>
            <form method="POST" action="'.$this->URI.'/coffret/liens">
            <label><b>Nom du titulaire :</b>
            <input type="text" name="name"  placeholder ="ex : Dupond" required/>
            </label>
             <label><b>Numéro de carte :</b>
            <input type="text" maxlength="16" placeholder ="ex : 1234567898765432" oninput="this.value=this.value.replace(/[^0-9]/g,\'\');" required/>
            </label>
             <label><b>CVV :</b>
            <input type="text" maxlength="3" placeholder ="ex : 123" oninput="this.value=this.value.replace(/[^0-9]/g,\'\');" required/>
            </label>
            <label><b>Mois d\'expiration :</b>
            <input type="number" name="mois" value="01" min="01" max="12" required >
            </label>
            <label><b>Année d\'expiration :</b>
            <input type="number" name="annee" value="2017" min="2017" max="3000" required>
            </label>';

                foreach ($_POST as $key => $value) {
                    $content .= '<input type="hidden" name="'.$key.'" value="' . $value . '" />';
                }
                $content .='<input type="submit"  value="Valider et payer"/></div></form><script type="text/javascript" src="web/js/gestionQuantite.js"></script>';
                break;
        }
        return $content;

    }

    public function renderRecapitulatif() {
        $formulaire = $this->htmlform();
        $nbGift=0;
        if(isset($_SESSION['coffret'])){
            foreach ($_SESSION['coffret'] as $produit=>$qt){
               $nbGift+=$qt;
            }
        }

        $content = <<<END
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
    </head>
   <body>
   <div class="test2">
        <h1>Giftbox</h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
       
        <a class="waves-effect waves-light btn-large" href="$this->URI/coffret"><span>Ma Giftbox&nbsp&nbsp</span> <i class="material-icons">redeem</i><span class="shad">&nbsp$nbGift</span></a>
        </div>  
              
              
                  
<div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>

        <table class="striped centered">
        <thead>
          <tr>
              <th data-field="id">Prestation</th>
              <th data-field="name">Prix Unitaire</th>
              <th data-field="price">Qté</th>
          </tr>
        </thead>

        <tbody>
        $formulaire
    </body>
    </html>
        

       
END;
        echo $content ;
        }




        public function renderCoffretFini($idCoff,$tokCadeau,$tokGestion){
                $idc = $_SESSION['infos']['idc'];
                $tc = $_SESSION['infos']['tc'];
                $tg = $_SESSION['infos']['tg'];
                $host = $_SERVER['HTTP_HOST'];
            $html = <<<END
            
            <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
   <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
         <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
        <div class="lien"><p>Merci pour votre achat.<br><br>Pour faire partager à la personne concernée, envoyé lui ce lien :
        <br><a href="http://$host$this->URI/$idCoff/cadeau?token=$tokCadeau">http://$host$this->URI/$idCoff/cadeau?token=$tokCadeau</a>
        <br><br>Si vous desirez modifier ou consulter ce coffret, vous pourrez le faire ici :<br>
        <a href="http://$host$this->URI/$idCoff/gerer?token=$tokGestion">http://$host$this->URI/$idCoff/gerer?token=$tokGestion</a></p></div>
  
   </body>
</html>
END;
        echo $html;
        }

// affichage du coffret cadeau en cours
    public function htmlCadeau($listPresta , $test , $idc, $statutP){
        $content ="<p id='descrCadeau'>Ce cadeau vous a été offert par $test->prenom $test->nom.<br><br>Cette personne voulait vous faire parvenir ce message :<br>$test->mess</p><br><ul class=\"collapsible popout\" data-collapsible=\"accordion\"><br>";
        foreach ($listPresta as $key=>$value){
            $p = Prestation::donnerPrestation($key);
            $content.=
            "<li><br>
            <div class=\"collapsible-header\"><span class=\"badge\" >Quantité : $value &nbsp</span><i class=\"material-icons\">redeem</i>$p->nom</div><br>
            <div class=\"collapsible-body\">
            <div class=\"pres\"><img src=\"$this->URI/web/img/$p->img\" style =\"width:100%;height:60%\"></div>
            <div class='pres2'><h4><b>$p->nom</b></h4>    <p>$p->descr</p> <br>";



            if(strcmp($statutP[$key],"Ouvert") !== 0){
                $content.="<p>Cliquez sur le bouton ci-dessous pour notifier à la personne vous ayant offert cette Giftbox la visualisation de cette prestation.<br>Cela permet à cette personne de suivre l'état de votre Giftbox</p>
            
            <a class=\"waves-effect waves-light btn\" href=\"$this->URI/$idc/cadeau/vu?token=$test->code_coffretOffert&idp=$p->id\" >Marquer comme vu</a></div></div></div><br>
            </li><br>";

        
            }else{
                $content.="<p class=><i>Vous avez deja visualisé cette prestation</i></p></div></div></div><br>
            </li><br>";
            }
        }


        return $content ;
    }



    public function renderCadeau($listPresta , $test , $idc, $prestaQ){
        if ($test->openDate <= date('Y-m-d')) {
            $content = $this->htmlCadeau($listPresta, $test, $idc, $prestaQ);
        }else{
            $d = explode('-',$test->openDate);
            $a = $d[0];
            $m = $d[1];
            $j = $d[2];
            $content = "Vous avez bel et bien un cadeau, mais vous ne pouvez pas l'ouvrir avant cette date:<br> $j/$m/$a";
        }
        $html = <<<END
        <!DOCTYPE html>
<html>
    <head>  
        <link type="text/css" rel="stylesheet" href="$this->URI/css/materialize.min.css"  media="screen,projection"/>
        <link href="$this->URI/css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
    </head>
    <body>
    <div class="test2">
        <h1><b>Giftbox</b></h1>
        </div>
    $content
    </body>
</html>
END;

    echo $html ;
    }

}