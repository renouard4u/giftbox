<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 04/01/2017
 * Time: 11:24
 */

namespace giftbox\views;


class CategorieView
{

    private $listecat;
    public $URI;
    private $route;

    public function __construct($o){
        $this->listecat = $o;
        $this->URI = \Slim\Slim::getInstance()->request->getRootUri();
        $this->route = '';
    }




    public function htmlCategories(){
        $affichage = '';
        foreach ($this->listecat as $res){
            $affichage .= '<a href='.$this->URI.'/categories/'.$res->id.'><div class="cat">'.$res->nom.'</div></a>';
        }
        $this->route = 'categories';
        return $affichage;
    }


    public function render(){
        $content = $this->htmlCategories();
        $nbGift=0;
        if(isset($_SESSION['coffret'])){
            foreach ($_SESSION['coffret'] as $produit=>$qt){
                $nbGift+=$qt;
            }

        }
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
   <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link href="css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
        <title>Giftbox</title>
        
       
    </head>
    <body>
        <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$this->URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$this->URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        
        <a class="waves-effect waves-light btn-large" href="$this->URI/coffret"><span>Ma Giftbox&nbsp&nbsp</span> <i class="material-icons">redeem</i><span class="shad">&nbsp$nbGift</span></a>
        </div>
        
        <div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$this->URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$this->URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$this->URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$this->URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
             
        $content        
        
    </body>
</html>
END;

        return $html;
    }
}