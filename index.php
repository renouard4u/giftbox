<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 13/12/2016
 * Time: 11:49
 */

session_start();

require 'vendor/autoload.php';

use giftbox\controllers\CatalogueController;
use giftbox\controllers\PrestationController;
use giftbox\controllers\CoffretController;
use giftbox\controllers\CategorieController;
use giftbox\controllers\CagnotteController;
use giftbox\controllers\GestionController;


\conf\Eloquent::init('src/conf/conf.ini');

$app = new \Slim\Slim;

$app->get('/',function(){
    $URI = \Slim\Slim::getInstance()->request->getRootUri();
    $nbGift=0;
    if(isset($_SESSION['coffret'])){
        foreach ($_SESSION['coffret'] as $produit=>$qt){
            $nbGift+=$qt;
        }

    }
    $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link href="css/app.css" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
         <title>Giftbox</title>
        
        
    </head>
    <body>
        
        <div class="test2">
        <h1><b>Giftbox</b></h1>
        <a class="waves-effect waves-light btn-large" href="$URI">Accueil</a>
        <a class="waves-effect waves-light btn-large" href="$URI/prestations">Nos prestations</a>
        <a class="waves-effect waves-light btn-large" href="$URI/categories">Nos categories</a>
        <a class="modal-trigger waves-effect waves-light btn-large" href="#mda">Qui sommes-nous ?</a>
        <a class="waves-effect waves-light btn-large" href="$URI/coffret"><span>Ma Giftbox&nbsp&nbsp</span> <i class="material-icons">redeem</i><span class="shad">&nbsp$nbGift</span></a>
       
        </div>  
        
      
<div id="mda" class="modal modal-fixed-footer bottom-sheet">
	<div class="modal-content">
		<h4>Qui sommes-nous ?</h4>
		<img src="$URI/web/img/clement.jpg" alt="" class="circle" style ="width:10%">
		<p><b>THOMAS Clément</b></p>
		<img src="$URI/web/img/quentind.jpg" alt="" class="circle" style ="width:10%">
		<p><b>DELAMARRE Quentin</b></p>
		<img src="$URI/web/img/quentinr.jpg" alt="" class="circle" style ="width:10%">
		<p><b>RENOUARD Quentin</b></p>
		<img src="$URI/web/img/paul.jpg" alt="" class="circle" style ="width:10%">
        <p><b>MERLIN Paul</b></p>		
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal-trigger').leanModal();
});
</script>
        
    <div class="slider">
    <ul class="slides">
      <li>
        <img src="web/img/img4.jpg" alt="">
        <div class="caption left-align">
          <h3>Faites plaisir à vos proches</h3>
         </div>
      </li>
      <li>
        <img src="web/img/img5.jpg" alt=""> 
        <div class="caption right-align">
          <h3>Réservez en toute simplicité</h3>
        </div>
      </li>
      <li>
        <img src="web/img/img6.jpg" alt="">
        <div class="caption center-align">
          <h3>Consultez rapidement l'évolution de vos cadeaux</h3>
        </div>
      </li>
      <li>
        <img src="web/img/img.jpg" alt="">
        <div class="caption right-align">
          <h3>Choisissez vous-même les dates d'ouverture</h3>
        </div>
      </li>
    </ul>
  </div>  
     <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>

        <!--Materializecss Slider-->
        <script>
            $(document).ready(function () {
                $('.slider').slider({full_width: true});
            });
        </script>
            </body>
</html>

END;


    echo $html;
});

$app->get('/prestations',function(){
    $con = new CatalogueController();
    $con->listePrestations();
});

$app->get('/prestations/:idp',function($idp){
   $con = new PrestationController();
    $con->detailPrestation($idp, false);
});

$app->get('/categories',function(){
    $con = new CategorieController();
    $con->listeCategories();
});

$app->get('/categories/:idc',function($idc){
   $con = new CatalogueController();
    $con->listePrestationsCategorieDonnee($idc);
});

$app->get('/prestations/ajout/:idp', function($idp){
    $con = new PrestationController();
    $con->ajouterPrestation($idp);
    $app = \Slim\Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/prestations/'.$idp);
});

$app->get('/coffret', function(){
    $con = new CoffretController();
    $con->listerCoffret();
})->name('coffret');

$app->get('/coffret/validation', function() {
    $con = new CoffretController();
    $con->donnerInfo();
});

$app->post('/coffret/validation', function() {
    $con = new CoffretController();
    $con->verifierInfo();
});

$app->post('/coffret/liens', function() {
   $con = new CoffretController();
    $con->enregistrerCoffret();
});

$app->get('/:idc/cadeau', function($idc){
    $con = new CoffretController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->afficherCadeau($idc, $tok);
});

$app->get('/:idc/cadeau/vu', function($idc){
   $con = new GestionController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $idp = $app->request->get('idp');
    $con->changerStatut($idc, $idp);
    $app->redirect($app->request->getRootUri()."/$idc/cadeau?token=$tok");
});

$app->get('/:idc/gerer', function($idc){
    $con = new GestionController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->demanderMdp($idc, $tok);
});

$app->post('/:idc/gerer',function($idc){
    $con = new GestionController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->verifierMdp($idc, $tok);
});

$app->get('/:idCa/cagnotte', function($idCa){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->participer($idCa, $tok);
});

$app->post('/:idCa/cagnotte', function($idCa){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->verifierParticipation($idCa, $tok);
});

$app->get('/:idCa/gererCagnotte', function ($idCa){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->demanderMdpCagnotte($idCa, $tok);
});

$app->post('/:idCa/gererCagnotte', function ($idCa){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con->verifierMdpCagnotte($idCa, $tok);
});


//Gestion cagnotte
$app->get('/:idCaGest/gererGagnotte/participer',function($idCaGest){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con -> participerGererCagnotte($idCaGest,$tok);
});

$app->post('/:idCaGest/gererGagnotte/participer',function($idCaGest){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con -> verifierParticipationGestion($idCaGest,$tok);
    $app->redirect($app->request->getRootUri()."/$idCaGest/gererCagnotte?token=$tok");
});

$app->get('/:idCaGest/gererGagnotte/cloturer',function($idCaGest){
    $con = new CagnotteController();
    $app = \Slim\Slim::getInstance();
    $tok = $app->request->get('token');
    $con -> cloturerCagnotte($idCaGest,$tok);
});



$app->run();